// RecDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GUIFilter.h"
#include "RecDlg.h"
#include "MainDlg.h"
#include "afxdialogex.h"

#include <mmsystem.h>

#include "libsrc/Convert.h"
#include "libsrc/Utils.h"

using namespace std;

// RecDlg dialog

IMPLEMENT_DYNAMIC(RecDlg, CDialog)

RecDlg::RecDlg(CWnd* pParent /*=NULL*/)
	: CDialog(RecDlg::IDD, pParent)
	, m_bStereo(FALSE)
	, m_bFormat(FALSE)
	, m_strSampleRate(_T("16000"))
	, m_strBPS(_T("16"))
	, m_bWrecked(false)
	, m_bHasTrack(false)
	, m_strBaseFreq(_T(""))
	, m_thrRecord(nullptr)
	, m_dlgParent((CMainDlg*) pParent)
	, m_iVolume(0)
	, m_correctBaseFreq(m_editBaseFreq, m_strBaseFreq, m_baseFreq, Filter::m_lowestFreq)
{

}

RecDlg::~RecDlg()
{
}

void RecDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_STEREO, m_bStereo);
	DDX_Check(pDX, IDC_FORMAT, m_bFormat);
	DDX_CBString(pDX, IDC_FS, m_strSampleRate);
	DDX_CBString(pDX, IDC_BPS, m_strBPS);
	DDX_Control(pDX, IDC_BPS, m_cmbBPS);
	DDX_Text(pDX, IDC_BASE_FREQ, m_strBaseFreq);
	DDX_Control(pDX, IDC_BASE_FREQ, m_editBaseFreq);
	DDX_Slider(pDX, IDC_HARMONIC_VOLUME, m_iVolume);
	DDX_Control(pDX, IDC_HARMONIC_COUNT, m_sliderHarmonics);
}


BEGIN_MESSAGE_MAP(RecDlg, CDialog)
	ON_BN_CLICKED(IDC_RECORD, &RecDlg::OnBnClickedRec)
	ON_BN_CLICKED(IDC_STOP, &RecDlg::OnBnClickedStop)
	ON_BN_CLICKED(IDC_FORMAT, &RecDlg::OnBnClickedForm)
	ON_BN_CLICKED(IDC_PLAY, &RecDlg::OnBnClickedPlay)
	ON_BN_CLICKED(IDC_WRECK, &RecDlg::OnBnClickedWreck)
	ON_BN_CLICKED(IDC_SAVE, &RecDlg::OnBnClickedSave)
	ON_EN_UPDATE(IDC_BASE_FREQ, &RecDlg::OnEnUpdateBaseFreq)
//	ON_NOTIFY(NM_CUSTOMDRAW, IDC_WRECK, &RecDlg::OnNMCustomdrawWreck)
//ON_BN_SETFOCUS(IDC_WRECK, &RecDlg::OnBnSetfocusWreck)
ON_WM_TIMER()
ON_BN_CLICKED(IDOK, &RecDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// RecDlg message handlers

void RecDlg::OnBnClickedRec()
{
	UpdateData();

	m_formatChunk = FORMATCHUNK(StrToInt(m_strBPS), m_bFormat ? WavMeta::FLOAT : WavMeta::PCM, StrToInt(m_strSampleRate), m_bStereo ? 2 : 1);

	m_correctBaseFreq.setHighLimit(m_formatChunk.m_SampleRate / 2.0F);

	GetDlgItem(IDC_RECORD)->EnableWindow(FALSE);
	GetDlgItem(IDC_PLAY)->EnableWindow(FALSE);
	GetDlgItem(IDC_STOP)->EnableWindow();
	GetDlgItem(IDC_SAVE)->EnableWindow();

	m_editBaseFreq.EnableWindow();

	//-------------------------RECORD------------------------
	TrackControls::InitWFX(m_wfx, m_formatChunk);

	MMRESULT result = waveInOpen(&m_waveIn, WAVE_MAPPER, &m_wfx, NULL, NULL, CALLBACK_NULL);
	ZeroMemory(&m_waveHeader, sizeof(m_waveHeader));

	m_waveHeader.dwBufferLength = m_wfx.nAvgBytesPerSec * 3;

	m_waveHeader.lpData = new char[m_waveHeader.dwBufferLength];
	ZeroMemory(m_waveHeader.lpData, m_waveHeader.dwBufferLength);

	m_recordedSamples.clear();										//uvolnim pre nove nahravanie
	m_bWrecked = false;


	m_thrRecord = AfxBeginThread(Record, this)->m_hThread;
}

uint32_t RecDlg::Record(LPVOID _this)
{
	ASSERT(_this);
	RecDlg& This = *static_cast<RecDlg*>(_this);

	MMRESULT result;

	Sleep(20);										//aby som nemal na zaciatku kopu nulovych vzoriek
	
		
	//result = waveInPrepareHeader(This.m_waveIn, &This.m_waveHeader, sizeof(WAVEHDR));
	//result = waveInAddBuffer(This.m_waveIn, &This.m_waveHeader, sizeof(WAVEHDR));				//nezacne nahravat kym mu nedam m_buffer

	while (This.m_recordedSamples.size() < This.m_waveHeader.dwBufferLength*20)						//po 20s skonci sam
	{
		result = waveInPrepareHeader(This.m_waveIn, &This.m_waveHeader, sizeof(This.m_waveHeader));
		result = waveInAddBuffer(This.m_waveIn, &This.m_waveHeader, sizeof(This.m_waveHeader));				//nezacne nahravat kym mu nedam m_buffer
		result = waveInStart(This.m_waveIn);													//nahrava asynchronne
	
		while (!(This.m_waveHeader.dwFlags & WHDR_DONE))											//nechod dalej kym vsetko nenaplnis			
			//TRACE("%d\n", This.m_waveHeader.dwBytesRecorded)
			;

		This.m_recordedSamples.insert(This.m_recordedSamples.end(), This.m_waveHeader.lpData, This.m_waveHeader.lpData + This.m_waveHeader.dwBufferLength);		//z buffra davame na koniec vektora
		result = waveInReset(This.m_waveIn);
		result = waveInUnprepareHeader(This.m_waveIn, &This.m_waveHeader, sizeof(This.m_waveHeader));
	}
	
	This.StopRecording();
	
	return 0;
}

void RecDlg::OnBnClickedStop()
{
	StopRecording();
}


void RecDlg::StopRecording()
{
	if (m_thrRecord)
	{	
		TerminateThread(m_thrRecord, 0);
		m_thrRecord = nullptr;

		GetDlgItem(IDC_RECORD)->EnableWindow();
		GetDlgItem(IDC_PLAY)->EnableWindow();
		GetDlgItem(IDC_WRECK)->EnableWindow();
	
		waveInStop(m_waveIn);
		waveInClose(m_waveIn);
	
		m_recordedSamples.insert(m_recordedSamples.end(), m_waveHeader.lpData, m_waveHeader.lpData + m_waveHeader.dwBytesRecorded);		//z buffra davame na koniec vektora
		m_floatSamples = Convert::toFloat(m_formatChunk, m_recordedSamples, true);
		
		delete m_waveHeader.lpData;										//ZeroMemory vynuluje lpData => bez delete je memory leak

		m_bHasTrack = true;
	}
	
	waveOutReset(m_waveOut);
}

const std::vector<uint8_t>& RecDlg::getSamplesToPlay() const
{
	return m_bWrecked ? m_wreckedSamples : m_recordedSamples;
}


void RecDlg::OnBnClickedForm()
{
	UpdateData(TRUE);

	if (m_bFormat)
	{
		m_strBPS = "32";
		UpdateData(FALSE);
	}
	
	m_cmbBPS.EnableWindow(!m_bFormat);		//enable if not float
}


void RecDlg::OnBnClickedPlay()
{
	//GetDlgItem(IDC_REC)->EnableWindow(FALSE);
	//m_wfx=WAVEFORMATEX_init(m_formatChunk, 1);

	//playSamples=floatTo(m_formatChunk.m_BitsPerSample, m_formatChunk.m_Format, normuj(m_floatSamples));
	waveOutReset(m_waveOut);

	MMRESULT res = waveOutOpen(&m_waveOut, WAVE_MAPPER, &m_wfx, 0, 0, CALLBACK_NULL);
	m_waveHeader.dwFlags = 0;
	m_waveHeader.dwBufferLength = misc::vectorByteSize(getSamplesToPlay());
	m_waveHeader.lpData = (LPSTR)getSamplesToPlay().data();

	res = waveOutPrepareHeader(m_waveOut, &m_waveHeader, sizeof(m_waveHeader));
	res = waveOutWrite(m_waveOut, &m_waveHeader, sizeof(m_waveHeader));
}


void RecDlg::OnBnClickedWreck()
{
	if (!m_correctBaseFreq.validateAndCorrect(IDC_TOOLTIP_TIMER, 1400, L"Wrong Input"))
		return;

	if (m_iVolume <= 0)
	{
		m_bWrecked = false;
		return;
	}

	try
	{
		m_wreckedSamples = Utils::addSineWave(m_iVolume / 100.0, m_baseFreq, m_floatSamples, m_formatChunk, m_sliderHarmonics.GetPos());
		m_bWrecked = true;
	}
	catch (exception& ex)
	{
		MB_SHOW_EX;
	}
}


void RecDlg::OnEnUpdateBaseFreq()
{	
	if (m_correctBaseFreq.validateAndCorrect(IDC_TOOLTIP_TIMER, 1400, L"Wrong Input") && m_correctBaseFreq.checkBounds())
	{
		int maxHarmonics = StrToInt(m_strSampleRate) / 2 / m_correctBaseFreq.getValue();
		m_sliderHarmonics.SetRangeMax(maxHarmonics, TRUE);
		m_sliderHarmonics.SetPos(maxHarmonics);

		m_sliderHarmonics.EnableWindow(TRUE);
	}
	else
		m_sliderHarmonics.EnableWindow(FALSE);
}


void RecDlg::OnBnClickedSave()
{
	CFileDialog dlgSave(FALSE, L".wav", NULL, FILE_DLG_ARGS);
	
	if (dlgSave.DoModal() == IDOK)
	{
		CString fName = dlgSave.GetPathName();
		
		if (!Utils::saveBuff(CStringToString(fName), m_formatChunk, getSamplesToPlay()))
			MessageBox(L"File couldn't be saved", L"Saving Error", MB_ICONERROR);
	}
}


//void RecDlg::OnBnSetfocusWreck()
//{
//	if (!GetDlgItem(IDC_WRECK)->IsWindowEnabled())
//	m_editBaseFreq.ShowBalloonTip(L"Enter input here", L"Enter input here", TTI_WARNING);
//}


void RecDlg::OnTimer(UINT_PTR nIDEvent)
{
	CDialog::OnTimer(nIDEvent);
}


BOOL RecDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Add extra initialization here
	if (m_bHasTrack)
	{
		GetDlgItem(IDC_WRECK)->EnableWindow();
		GetDlgItem(IDC_SAVE)->EnableWindow();
		
		m_editBaseFreq.EnableWindow();

		if (m_correctBaseFreq.checkBounds())
		{
			m_sliderHarmonics.EnableWindow(TRUE);
		}
	}

	m_sliderHarmonics.SetRangeMin(1);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}


void RecDlg::OnBnClickedOk()
{
	try
	{
		m_dlgParent->LoadWav(WavMeta(m_formatChunk, getSamplesToPlay()));

		CDialog::OnOK();
	}
	catch (exception& ex)
	{
		MB_SHOW_EX;
	}
}
