#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "libsrc/parser.h"
#include "libsrc/WaveFilter.h"

// CFilterDlg dialog

class CFilterDlg : public CDialogEx							//trieda CFilterDlg
{
	DECLARE_DYNAMIC(CFilterDlg)

public:
	CFilterDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFilterDlg();

// Dialog Data
	enum { IDD = IDD_FILTER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:	
	class FilterParams
	{
	public:
		double m_baseFreq = 0;
		float m_slope = 0.99F;
		BOOL m_bIIR = FALSE;
		BOOL m_bExpressionType = WaveFilter::FREQUENCY;

		CString m_strExpression;
				
		bool operator==(const FilterParams& other) const;
	};

	FilterParams m_filterParams, m_lastFilterParams;

	CString m_strPrevExpression;

	Parser::vectorMap symsMap;
	CButton m_checkIIR;
	Parser m_parser;

	bool m_finishedOkOnce;

	void Finish();
	void processParseException(const ParserException &ex);		//TODO: do it smarter


	void SwitchControlsState();

	CRichEditCtrl m_editExpression;
	CString m_strBaseFreq, m_strSlope;
	CSpinButtonCtrl m_spinSlope;
	CEdit m_editBaseFreq, m_editSlope;
		
	CStatic m_statExprStatus;
	CRichEditCtrl m_editExprStatus;

	CHARFORMAT2 FORMAT_RED, FORMAT_BLACK, FORMAT_GREEN;

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnEnUpdateEditbasefreq();
	afx_msg void OnBnClickedOk();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedcheckiir();	
	afx_msg void OnEnDropfilesEditCustExpr(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnEnChangeEditCustExpr();
	afx_msg void OnEnChangeeditslope();
	
	
	bool m_lastFilterOK;

	DecimalCorrector m_correctBaseFreq;
		
	vector<float> filterWav(const WavMeta& wav);
};
