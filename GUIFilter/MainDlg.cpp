
// achjajDlg.cpp : implementation file
//

#include "stdafx.h"

#include <mmsystem.h>

#include "GUIFilter.h"
#include "WavTrack.h"
#include "libsrc/Utils.h"
#include "libsrc/filter.h"

#include "FilterDlg.h"
#include "RecDlg.h"
#include "ConvertDlg.h"
#include "MainDlg.h"

#include "afxdialogex.h"

using namespace std;
using namespace misc;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMainDlg dialog




CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)					//premenne riadiacich prvkov
	: CDialogEx(CMainDlg::IDD, pParent)
	, m_lastGoodPath(_T(""))
	, m_strOrigProgress(_T(""))
	, m_strFilteredProgress(_T(""))
	, m_strDisplayFileName(_T(""))
	, m_dlgRecord(this)
	, m_dlgConvert(this)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_sliderVOLUME, m_sliderVolume);

	DDX_Text(pDX, IDC_ST_PlayProgOld, m_strOrigProgress);
	DDX_Text(pDX, IDC_FILENAME, m_strDisplayFileName);
	DDX_Text(pDX, IDC_ST_PlayProgNew, m_strFilteredProgress);
	DDX_Control(pDX, IDC_FILTER, m_btFilter);
}

BEGIN_MESSAGE_MAP(CMainDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_OPEN, &CMainDlg::OnBnClickedBrowse)
	ON_BN_CLICKED(IDC_INFO, &CMainDlg::OnBnClickedInfo)
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_SAVE, &CMainDlg::OnBnClickedSave)
	ON_BN_CLICKED(IDC_FILTER, &CMainDlg::OnBnClickedFilter)
	ON_BN_CLICKED(IDOK, &CMainDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_PLAYOLD, &CMainDlg::OnBnClickedPLAYOLD)
	ON_BN_CLICKED(IDC_PAUSEOLD, &CMainDlg::OnBnClickedPAUSEOLD)
	ON_BN_CLICKED(IDC_STOPOLD, &CMainDlg::OnBnClickedSTOPOLD)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_PLAYNEW, &CMainDlg::OnBnClickedPlaynew)
	ON_BN_CLICKED(IDC_PAUSENEW, &CMainDlg::OnBnClickedPausenew)
	ON_BN_CLICKED(IDC_STOPNEW, &CMainDlg::OnBnClickedStopnew)
	ON_BN_CLICKED(IDC_RECORD, &CMainDlg::OnBnClickedRecord)
	ON_BN_CLICKED(IDC_CONV_DLG, &CMainDlg::OnBnClickedConvDlg)
	ON_MESSAGE(WM_APP_UPDATE, &CMainDlg::UpdateData_Thread)
END_MESSAGE_MAP()


// CMainDlg message handlers

BOOL CMainDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_sliderVolume.SetRange(0, 0xFFFF);			//rozsah hlasitosti
	m_sliderVolume.SetPos(0.7*0xFFFF);				//initial volume

	m_trackOriginal = TrackControls(GetDlgItem(IDC_PLAYOLD), GetDlgItem(IDC_PAUSEOLD), GetDlgItem(IDC_STOPOLD), &m_strOrigProgress, this);
	m_trackFiltered = TrackControls(GetDlgItem(IDC_PLAYNEW), GetDlgItem(IDC_PAUSENEW), GetDlgItem(IDC_STOPNEW), &m_strFilteredProgress, this);
	
		
	m_bPlaybacksReady = false;									//otvorenie vystupnych audio streamov
	m_bHadGoodPlaybacks = false;								//pokial dobre neotvoril subor, ostane false

	saved = false;								//subor sa neprepisal
	
	UpdateData(FALSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMainDlg::OnSysCommand(uint32_t nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model, 

//  this is automatically done for you by the framework.

void CMainDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMainDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMainDlg::OnBnClickedBrowse()
{
	CFileDialog dlgOpen(TRUE, NULL, NULL, FILE_DLG_ARGS);
	
	if (dlgOpen.DoModal() == IDOK)
		LoadFile(dlgOpen.GetPathName());
}


bool CMainDlg::PrepareWav(WavMeta & wavMeta)
{
	bool goodFile = false;																		//assume false


	m_trackOriginal.InitWFX(wavMeta.m_wavHead.m_fmtChunk);										//skusime otvorit kartu pre tento track
	m_trackFiltered.InitWFX(wavMeta.m_wavHead.m_fmtChunk, true);										//a pre novy track

	//1. Prepare new file
	m_bPlaybacksReady = m_trackFiltered.Open() == MMSYSERR_NOERROR && m_trackOriginal.Open() == MMSYSERR_NOERROR;

	//2. If failed, open last good file, if any
	if (!m_bPlaybacksReady)
	{
		if (m_bHadGoodPlaybacks)
		{
			int mbRes = MessageBox(L"Failed to open audio device!\nLoad last valid file?", L"Error", MB_ICONQUESTION | MB_YESNO);

			if (mbRes == IDYES)
			{
				m_trackOriginal.InitWFX(m_wavMetaGood.m_wavHead.m_fmtChunk, false);				//skusime otvorit kartu pre dobry track
				m_trackFiltered.InitWFX(m_wavMetaGood.m_wavHead.m_fmtChunk, true);				//a pre novy track

				m_bPlaybacksReady = m_trackFiltered.Open() == MMSYSERR_NOERROR &&m_trackOriginal.Open() == MMSYSERR_NOERROR;
			}
			else
				m_lastGoodPath.Empty();
		}
	}
	else
	{
		m_wavMetaGood = std::move(wavMeta);								//dobry wav sa do buducna ulozi
		m_dlgFilter.m_correctBaseFreq.setHighLimit(m_wavMetaGood.m_wavHead.m_fmtChunk.m_SampleRate / 2);
		goodFile = true;						//subor bol dobry
	}								//po kazdom otvoreni kariet treba vsetko nanovo


	if (m_bPlaybacksReady)
	{
		m_bHadGoodPlaybacks = true;								//dobre otvoril aspon 1 subor
		m_dlgFilter.m_lastFilterOK = false;						//novy subor sa este nefiltroval 

		m_trackOriginal.SetVolume(m_sliderVolume);				//works for both tracks...

		m_trackOriginal.Prepare(m_wavMetaGood.getByteVec());

		MessageBox(L"File loaded", L"Success", MB_ICONINFORMATION);
		updateControlsState(TRUE);
	}
	else
	{
		MessageBox(L"Failed to open audio device!", L"Error", MB_ICONSTOP);
		updateControlsState(FALSE);
	}


	return goodFile;
}


void CMainDlg::LoadFile(const CString& filePath)
{
	if (!saved && m_lastGoodPath == filePath)	//nepovoli otvorit znova rovnaky subor, iba ak by sa prepisal
	{
		MessageBox(L"File already opened!", L"Already opened", MB_ICONSTOP);
		return;
	}

	saved = false;												//otvaranie prepisaneho
			
	WavFileManager::fileTimePair fileTimePair;

	if (wavFileManager.isBad(filePath, &fileTimePair))
	{
		MessageBox(L"Trying to open invalid file!", L"Invalid file", MB_ICONWARNING);
		return;																				//tento wav je zly
	}
	

	WavMeta wave;

	try
	{
		wave.loadFile(CStringToString(filePath));
	}
	catch (WavException& ex)
	{
		wavFileManager.insert(fileTimePair);
		
		CString caption = ex.getStatus() == WavStatus::BAD_HEADER ? L"Invalid file" : L"Invalid data";

		MB_SHOW_EX;
		return;
	}
	

	bool isOK = PrepareWav(wave);
						
	if (isOK)
		m_lastGoodPath = filePath;					//m_lastGoodPath je cesta posledneho dobreho wavu
	else											//ak je otvoreny subor zly a neni v databaze
		wavFileManager.insert(fileTimePair);

	if (m_lastGoodPath.IsEmpty())
		m_strDisplayFileName.Empty();
	else
		m_strDisplayFileName = PathFindFileName(filePath);
						
	UpdateData(FALSE);										//updatne display z premien
}

void CMainDlg::LoadWav(WavMeta & wavMeta)
{
	bool isOK = PrepareWav(wavMeta);

	if (isOK)
	{
		m_strDisplayFileName.Empty();
		UpdateData(FALSE);										//updatne display z premien
	}
}


void CMainDlg::updateControlsState(BOOL UP)
{
	OnBnClickedSTOPOLD();							//zapne PLAY, vypne STOP, PAUSE a uvolni stare vzorky
	OnBnClickedStopnew();							//zapne PLAY, vypne STOP, PAUSE a uvolni nove vzorky
	
	GetDlgItem(IDC_PLAYNEW)->EnableWindow(FALSE);		//este vypne playnew
	GetDlgItem(IDC_SAVE)->EnableWindow(FALSE);

	GetDlgItem(IDC_INFO)->EnableWindow(UP);
	GetDlgItem(IDC_sliderVOLUME)->EnableWindow(UP);
	m_btFilter.EnableWindow(UP);
	GetDlgItem(IDC_CONV_DLG)->EnableWindow(UP);

	if (!UP)
		GetDlgItem(IDC_PLAYOLD)->EnableWindow(FALSE);		//este vypne playold
}


void CMainDlg::OnBnClickedInfo()
{
	/*info << "Sample frequency: " << m_wavHead.m_fmtChunk.m_SampleRate << " Hz\nBits per sample: " << m_wavHead.m_fmtChunk.m_BitsPerSample <<
		"\nFormat: " << getVawFormats()[formatIndex] << "\nLength: " << m_length << " ms\nChannels: " << m_wavHead.m_fmtChunk.m_Channels <<
		"\nNo. of samples: "<< m_size << "\nSize: " << m_fileSize << " B";*/
	auto& fmtChunk = m_wavMetaGood.m_wavHead.m_fmtChunk;
		
	CString info(fmtChunk.print().data());
		
	info.Format(L"Sample frequency: %d Hz\nBits per sample: %d\nFormat: %s\nLength: %d ms\nChannels: %d\nNo. of samples: %d\nSize: %d B",
		fmtChunk.m_SampleRate, fmtChunk.m_BitsPerSample, CString(fmtChunk.getFormatInfo().data()), m_wavMetaGood.getTrackTime(), fmtChunk.m_Channels, m_wavMetaGood.getTotalSamples(), m_wavMetaGood.getFileSize());
		
	MessageBox(info, L"Track Info", MB_OK | MB_ICONINFORMATION);
}


void CMainDlg::OnBnClickedFilter()
{	
	if (m_dlgFilter.DoModal() == IDOK)								//stlacil OK.ak zadal zle, cyklus pokracuje
	{		
		try
		{
			m_vecFiltered = m_dlgFilter.filterWav(m_wavMetaGood);

			if (m_vecFiltered.empty())										//vektor je prazdny lebo sa neda prehrat
				MessageBox(L"Filtered sound unplayable!", L"Error", MB_ICONSTOP);
			else if (m_bPlaybacksReady)												//dobre sa otvorilo a ofiltrovalo
			{
				m_trackFiltered.Prepare(m_vecFiltered);

				m_dlgFilter.m_lastFilterOK = true;//pokial neda novy subor, alebo nezmeni niektore parametre, to iste znova nefiltruje

				MessageBox(L"Done", L"Done", MB_ICONINFORMATION);								//vse hotovo
				OnBnClickedStopnew();									//pripravi novy trek na prehravanie
				GetDlgItem(IDC_SAVE)->EnableWindow();								//zapne gombik save
			}
		}
		catch (const std::exception& ex)
		{
			MB_SHOW_EX;
		}
	}
}


void CMainDlg::OnBnClickedOk()
{
	//mciSendString(L"close myFile", NULL, 0, 0);				//zavre MCI

	m_trackOriginal.Close();
	m_trackFiltered.Close();

	CDialogEx::OnOK();
}


void CMainDlg::OnHScroll(uint32_t nSBCode, uint32_t nPos, CScrollBar* pScrollBar)
{	
	m_trackFiltered.SetVolume(m_sliderVolume);					//STRANGE - works for both tracks...
	
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CMainDlg::OnBnClickedPLAYOLD()
{
	m_trackOriginal.Play();
}


void CMainDlg::OnBnClickedPAUSEOLD()
{
	m_trackOriginal.Pause();
}


void CMainDlg::OnBnClickedSTOPOLD()
{
	m_trackOriginal.Stop();
}


void CMainDlg::OnBnClickedPlaynew()
{
	m_btFilter.EnableWindow(FALSE);
	m_trackFiltered.Play();
}


void CMainDlg::OnBnClickedPausenew()
{
	m_trackFiltered.Pause();											//aby sa resumla
}


void CMainDlg::OnBnClickedStopnew()
{
	m_trackFiltered.Stop();
	m_btFilter.EnableWindow();
}


void CMainDlg::OnBnClickedSave()
{
	CFileDialog dlgSave(FALSE, L".wav", NULL, FILE_DLG_ARGS);//okno pre save

	if (dlgSave.DoModal() == IDOK)										//stlacil OK
	{
		CString mbText;
		mbText.Format(L"Save in native [%s] format?", CString(m_wavMetaGood.m_wavHead.m_fmtChunk.getFormatInfo().data()));

		auto mbRes = MessageBox(mbText, L"Choose format", MB_ICONQUESTION | MB_YESNOCANCEL);

		if (mbRes != IDCANCEL)
		{
			bool bAsFloat = mbRes == IDNO;

			CString savePath = dlgSave.GetPathName();								//cesta k novemu suboru
			int status = Utils::writeWav(CStringToString(savePath), m_vecFiltered, m_wavMetaGood.m_wavHead, bAsFloat);		//true = >ulozi float

			if (status == -1)														//neotvoril subor
				MessageBox(L"Can't write file.\nMaybe it's currently opened", L"Error", MB_ICONSTOP);
			else
			{
				MessageBox(status ? L"Couldn't save in native type.\nSaved in FLOAT32 format." : L"Saved", L"Saved", MB_ICONINFORMATION);

				wavFileManager.erase(savePath);									//ak je prave ulozeny filename v databaze, vymazeme ho
			}

			saved = true;						//subor sa ukladal.ak bol otvoreny a pokusi sa ho otvorit, dovoli mu to
		}
	}
}


void CMainDlg::OnBnClickedRecord()
{
	m_dlgRecord.DoModal();
}


void CMainDlg::OnBnClickedConvDlg()
{
	m_dlgConvert.DoModal();
}


LRESULT CMainDlg::UpdateData_Thread(WPARAM wpD, LPARAM lpD)
{
	UpdateData(FALSE);
	return 0;
}