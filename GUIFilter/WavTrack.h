#pragma once

#include <string>
#include <vector>
#include <mmsystem.h>

struct FORMATCHUNK;

class TrackControls
{
public:
	TrackControls();
	TrackControls(CWnd* btPlay, CWnd* btPause, CWnd* btStop, CString* strOutput, CWnd* pWnd);
	~TrackControls(void);
	
private:
	HWAVEOUT m_hWaveOut;
	WAVEHDR m_WaveHdr;
	WAVEFORMATEX m_wfx;
	MMTIME m_mmTime;

	CWnd *m_btPlay, *m_btPause, *m_btStop;
	CWnd *m_pWindow;
	UINT_PTR m_timer;
		
	CString* m_strOutput;
	
	bool m_bPaused;
	
	//WavMeta wave;
	CWinThread* m_threadFunc;

	LPSTR m_buffer;
	DWORD m_bufferLength;
	bool m_bPlayOrPause;
	
	void OnStop();
	void OnPlayButton();

public:
	static void InitWFX(WAVEFORMATEX& wfx, const FORMATCHUNK& fmtChunk, bool isFloat = false);
	void InitWFX(const FORMATCHUNK& fmtChunk, bool isFloat = false);

	void SetVolume(CSliderCtrl& slider);
	void SetVolume(DWORD volume);

	void Play();
	void Pause();
	void Stop();
	static UINT showTime(LPVOID arg);

	void setBuffer(LPSTR buffer, DWORD length);

	UINT playTrack(LPVOID _this);

	template<typename T> MMRESULT Prepare(const std::vector<T>& samples);
	MMRESULT Prepare(LPSTR lpData, DWORD dwBufferLength);
	MMRESULT Prepare();
	
	MMRESULT Open();
	MMRESULT Close();

	template<typename T> MMRESULT InitOpenPrepare(const FORMATCHUNK& fmtChunk, const std::vector<T>& samples);
};


template<typename T>
inline MMRESULT TrackControls::Prepare(const std::vector<T>& samples)
{
	return Prepare((LPSTR)samples.data(), misc::vectorByteSize(samples));
}

template<typename T>
inline MMRESULT TrackControls::InitOpenPrepare(const FORMATCHUNK & fmtChunk, const std::vector<T>& samples)
{
	InitWFX(fmtChunk);

	auto result = Open();

	if (result == MMSYSERR_NOERROR)
		result = Prepare(samples);

	return result;
}
