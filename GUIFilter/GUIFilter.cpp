
// achjaj.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "GUIFilter.h"
#include "MainDlg.h"

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CachjajApp

BEGIN_MESSAGE_MAP(CachjajApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CachjajApp construction

CachjajApp::CachjajApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CachjajApp object

CachjajApp theApp;


// CachjajApp initialization
#define CRTDBG_MAP_ALLOC
BOOL CachjajApp::InitInstance()
{
	_CrtDumpMemoryLeaks();
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	AfxInitRichEdit2();

	CWinApp::InitInstance();


	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CMainDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	
	return FALSE;
}

/*
#define AFX_IDP_PARSE_INT               0xF110
#define AFX_IDP_PARSE_REAL              0xF111
#define AFX_IDP_PARSE_INT_RANGE         0xF112
#define AFX_IDP_PARSE_REAL_RANGE        0xF113
#define AFX_IDP_PARSE_STRING_SIZE       0xF114
#define AFX_IDP_PARSE_RADIO_BUTTON      0xF115
#define AFX_IDP_PARSE_BYTE              0xF116
#define AFX_IDP_PARSE_UINT              0xF117
#define AFX_IDP_PARSE_DATETIME          0xF118
#define AFX_IDP_PARSE_CURRENCY          0xF119
#define AFX_IDP_PARSE_GUID              0xF11A
#define AFX_IDP_PARSE_TIME              0xF11B
#define AFX_IDP_PARSE_DATE              0xF11C
*/


//int CachjajApp::DoMessageBox(LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt)
//{
//	CWnd* pWin = AfxGetMainWnd()->GetActiveWindow();
//
//	CFilterDlg* pFilterDlg = dynamic_cast<CFilterDlg*>(pWin);
//	
//
//	if (nIDPrompt == 0xF111)
//		return 0;
//
//	return CWinApp::DoMessageBox(lpszPrompt, nType, nIDPrompt);
//}


bool validNumber(const CString & str, double limitDown, double limitUp, double * pValue)
{
	if (str.IsEmpty())
		return false;

	double result = 0;
	CT2CA strDbl(str);
			
	if (!Utils::strToDouble(strDbl, result))
		return false;

	if (limitUp && result > limitUp)
		return false;

	if (limitDown && result < limitDown && str.Find('.') > 0)
		return false;

	if (pValue)
		*pValue = result;

	return true;
}


string CStringToString(const CString& str)								//TU NESMIE BYT SMERNIK, LEBO SI MYSLI ZE VOLAM PRETAZENY CTOR S BOOL
{
	return string(CT2CA(str));
}


void CorrectEditBox(CEdit& edit, CString& str)
{
	//edit.Undo();															//does infinite loop
	
	DWORD pos = HIWORD(edit.GetSel()) - 1;												//pozicia vadneho prvku-1
	str.Delete(pos, 1);
	edit.GetParent()->UpdateData(FALSE);											//update sa string do edit boxu
	edit.SetSel(pos, pos, 0);														//nastavime polohu kurzora
}

void initTextColor(CHARFORMAT2& format, COLORREF color)
{
	format.cbSize = sizeof(CHARFORMAT2);
	format.dwMask = CFM_COLOR;
	format.dwEffects = ~CFE_AUTOCOLOR;							//doplnok - vsetko okrem CFE_AUTOCOLOR
	format.crTextColor = color;
}



DecimalCorrector::DecimalCorrector::DecimalCorrector(CEdit & control, CString & sText, double & resultVal, double lowLimit, double highLimit) :
	m_control(control),
	m_sText(sText),
	m_resultVal(resultVal),
	m_lowLimit(lowLimit),
	m_highLimit(highLimit)
{
	createTip();
}

void DecimalCorrector::setLowLimit(double lowLimit)
{
	m_lowLimit = lowLimit;
	createTip();
}

void DecimalCorrector::setHighLimit(double highLimit)
{
	m_highLimit = highLimit;
	createTip();
}

bool DecimalCorrector::validate()
{
	return validNumber(m_sText, m_lowLimit, m_highLimit, &m_resultVal);
}

bool DecimalCorrector::checkBounds() const
{
	return m_resultVal >= m_lowLimit && m_resultVal <= m_highLimit;
}

void DecimalCorrector::createTip()
{
	m_sTip.Format(L"Enter decimal number <%.0f, %.0f>", m_lowLimit, m_highLimit);
}

void DecimalCorrector::showTip(LPCWSTR balloonTitle, int icon)
{
	m_control.ShowBalloonTip(balloonTitle, m_sTip, icon);
}

void DecimalCorrector::correct()
{
	CorrectEditBox(m_control, m_sText);
}

double DecimalCorrector::getValue() const
{
	return m_resultVal;
}

bool DecimalCorrector::validateAndCorrect(UINT_PTR nIDEvent, UINT nElapse, LPCWSTR balloonTitle)
{
	auto parent = m_control.GetParent();
	
	parent->UpdateData(TRUE);				//updates variable

	auto ok = validate();

	if (!ok)
	{
		parent->SetTimer(nIDEvent, nElapse, NULL);

		showTip(balloonTitle);
		correct();
	}

	return ok;
}
