// FilterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GUIFilter.h"
#include "FilterDlg.h"
#include "afxdialogex.h"

#include "libsrc/filter.h"

#include <fstream>

using namespace std;

// CFilterDlg dialog

IMPLEMENT_DYNAMIC(CFilterDlg, CDialogEx)

	CFilterDlg::CFilterDlg(CWnd* pParent /*=NULL*/)					//tieto hodnoty sa naplnia iba raz
	: CDialogEx(CFilterDlg::IDD, pParent)
	, m_strBaseFreq(_T(""))
	, m_finishedOkOnce(false)
	,m_correctBaseFreq(m_editBaseFreq, m_strBaseFreq, m_filterParams.m_baseFreq, Filter::m_lowestFreq)
{
	initTextColor(FORMAT_RED, RGB(255, 0, 0));
	initTextColor(FORMAT_BLACK, 0);
	initTextColor(FORMAT_GREEN,  RGB(0, 128, 0));
}

CFilterDlg::~CFilterDlg()
{
}

void CFilterDlg::DoDataExchange(CDataExchange* pDX)				//zapne ich IDOK a UpdateData
{
	CDialogEx::DoDataExchange(pDX);

	DDV_MaxChars(pDX, m_strBaseFreq, 20);

	DDX_Control(pDX, IDC_EditBaseFreq, m_editBaseFreq);
	DDX_Text(pDX, IDC_EditBaseFreq, m_strBaseFreq);

	DDX_Control(pDX, IDC_spinSlope, m_spinSlope);
	DDX_Control(pDX, IDC_editSlope, m_editSlope);

	DDX_Check(pDX, IDC_checkIIR, m_filterParams.m_bIIR);

	DDX_Text(pDX, IDC_EDIT_CUST_EXPR, m_filterParams.m_strExpression);
	DDX_Control(pDX, IDC_EDIT_CUST_EXPR, m_editExpression);
	DDX_Radio(pDX, IDC_RADIO_BASE_FREQ, m_filterParams.m_bExpressionType);
	DDX_Control(pDX, IDC_checkIIR, m_checkIIR);

	DDX_Control(pDX, IDC_RICHEDIT_EXPR_STATUS, m_editExprStatus);
}


BEGIN_MESSAGE_MAP(CFilterDlg, CDialogEx)
	ON_BN_CLICKED(IDOK, &CFilterDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_RADIO_FREQ_EXPR, &SwitchControlsState)
	ON_BN_CLICKED(IDC_RADIO_BASE_FREQ, &SwitchControlsState)
	ON_BN_CLICKED(IDC_checkIIR, &CFilterDlg::OnBnClickedcheckiir)
	ON_WM_TIMER()
	ON_WM_KEYDOWN()
	ON_NOTIFY(EN_DROPFILES, IDC_EDIT_CUST_EXPR, &CFilterDlg::OnEnDropfilesEditCustExpr)
	//ON_EN_CHANGE(IDC_EDIT_CUST_EXPR, &CFilterDlg::OnEnChangeEditCustExpr)
	ON_EN_CHANGE(IDC_editSlope, &CFilterDlg::OnEnChangeeditslope)
	ON_EN_UPDATE(IDC_EditBaseFreq, &CFilterDlg::OnEnUpdateEditbasefreq)
END_MESSAGE_MAP()


// CFilterDlg message handlers


BOOL CFilterDlg::OnInitDialog()								//pri kazdom spusteni okna
{
	CDialogEx::OnInitDialog();

	// TODO:  Add extra initialization here
	SwitchControlsState();

	m_spinSlope.SetRange(1, 999);

	if (m_finishedOkOnce)														//mame hodnoty last tak ich dame do prvkov
	{
		m_spinSlope.SetPos(m_lastFilterParams.m_slope * 1000);
		m_strBaseFreq.Format(L"%f", m_lastFilterParams.m_baseFreq);
		m_strBaseFreq.TrimRight(L".0");										//oreze zbytocne nuly
	}
	else															//nemame hodnoty last tak dame prvotne
	{		
		m_spinSlope.SetPos(m_filterParams.m_slope * 1000);
		m_strBaseFreq.Empty();
	}
	
	m_editExpression.SetEventMask(ENM_DROPFILES | ENM_CHANGE);
	
	m_editExprStatus.SetDefaultCharFormat(FORMAT_RED);

	
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


void CFilterDlg::OnBnClickedcheckiir()
{
	UpdateData();

	m_editSlope.EnableWindow(m_filterParams.m_bIIR);
	m_spinSlope.EnableWindow(m_filterParams.m_bIIR);

	if (!m_filterParams.m_bIIR)
		m_editBaseFreq.SetFocus();										//da focus na edit control
}




void CFilterDlg::OnEnUpdateEditbasefreq()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function to send the EM_SETEVENTMASK message to the control
	// with the ENM_UPDATE flag ORed into the lParam mask.

	// TODO:  Add your control notification handler code here

	m_correctBaseFreq.validateAndCorrect(IDC_TOOLTIP_TIMER, 1400, L"Wrong Input");
}


void CFilterDlg::OnBnClickedOk()
{
	UpdateData(TRUE);


	if (m_lastFilterOK && m_filterParams == m_lastFilterParams)
	{
		MessageBox(L"Already done", L"Done", MB_ICONINFORMATION);
		return;
	}


	if (m_filterParams.m_bExpressionType == WaveFilter::ParamType::FREQUENCY)
	{
		if (!m_correctBaseFreq.checkBounds())
		{
			SetTimer(IDC_TOOLTIP_TIMER, 3000, NULL);													//trva 3s
			
			m_correctBaseFreq.showTip(L"Wrong Input");
			
			return;
		}
	}
	else// if (m_filterParams == m_lastFilterParams)
	{
		if (m_filterParams.m_strExpression.IsEmpty())
		{
			SetTimer(IDC_TOOLTIP_TIMER, 3000, NULL);													//trva 3s

			//m_editInvisible.ShowBalloonTip(L"No Input", L"Provide a symbolic expression, or drop a file", TTI_WARNING);			//mala / prazdna m_baseFreq
			m_editExprStatus.SetWindowTextW(L"No Input: Please provide a symbolic expression, or drop a file");
						
			m_strPrevExpression = m_filterParams.m_strExpression;

			return;
		}

		if (m_strPrevExpression == m_filterParams.m_strExpression)				//bad input 
			return;
		
		m_strPrevExpression = m_filterParams.m_strExpression;		//TODO: keep it here for now

		try
		{
			symsMap = m_parser.getVectorsMap(istringstream(CStringToString(m_filterParams.m_strExpression)));
		}
		catch (ParserException &ex)
		{
			processParseException(ex);

			return;
		}

		m_editExprStatus.SetWindowTextW(L"");
	}
	

	Finish();
}


void CFilterDlg::OnEnDropfilesEditCustExpr(NMHDR *pNMHDR, LRESULT *pResult)
{
	ENDROPFILES *pEnDropFiles = reinterpret_cast<ENDROPFILES *>(pNMHDR);
	// TODO:  The control will not send this notification unless you override the
	// CDialogEx::OnInitDialog() function to send the EM_SETEVENTMASK message
	// to the control with the ENM_DROPFILES flag ORed into the lParam mask.

	// TODO:  Add your control notification handler code here

	char filePath[MAX_PATH];
	DragQueryFileA((HDROP)pEnDropFiles->hDrop, 0, filePath, MAX_PATH);
	DragFinish((HDROP)pEnDropFiles->hDrop);

	ifstream file(filePath);

	try
	{
		symsMap = m_parser.getVectorsMap(file);

		file.clear();                 // clear fail and eof bits
		file.seekg(0, ios::beg); // back to the start!

		m_filterParams.m_strExpression.Empty();
		std::string str;

		while (std::getline(file, str))
			m_filterParams.m_strExpression += CString(str.data()) + '\n';

		UpdateData(FALSE);

		m_editExpression.SetSel(0, -1);
		m_editExpression.SetSelectionCharFormat(FORMAT_GREEN);
	}
	catch (const ParserException &ex)
	{
		processParseException(ex);
	}

	*pResult = 0;
}


void CFilterDlg::processParseException(const ParserException & ex)
{
	m_lastFilterOK = false;

	m_editExprStatus.SetWindowTextW(CString(ex.what()));

	m_editExpression.SetSel(0, -1);
	m_editExpression.SetSelectionCharFormat(FORMAT_BLACK);

	m_editExpression.SetSel(ex.m_position, -1);
	m_editExpression.SetSelectionCharFormat(FORMAT_RED);
}


void CFilterDlg::Finish()
{
	m_lastFilterParams = m_filterParams;

	m_finishedOkOnce = true;												//mame dobre hodnoty
	CDialogEx::OnOK();													//vypne dialog a updatne premenne
}

void CFilterDlg::OnTimer(UINT_PTR nIDEvent)
{
	m_editSlope.HideBalloonTip();
	m_editBaseFreq.HideBalloonTip();

	KillTimer(IDC_TOOLTIP_TIMER);
	
	CDialogEx::OnTimer(nIDEvent);
}


void CFilterDlg::SwitchControlsState()
{
	UpdateData();

	BOOL bFreq = m_filterParams.m_bExpressionType == 0;

	m_editBaseFreq.EnableWindow(bFreq);
	m_checkIIR.EnableWindow(bFreq);

	m_spinSlope.EnableWindow(bFreq && m_filterParams.m_bIIR);
	m_editSlope.EnableWindow(bFreq && m_filterParams.m_bIIR);

	m_editExpression.EnableWindow(!bFreq);
}


void CFilterDlg::OnEnChangeEditCustExpr()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

}

vector<float> CFilterDlg::filterWav(const WavMeta& wav)
{
	if (m_filterParams.m_bExpressionType == WaveFilter::ParamType::FREQUENCY)
		return Filter::filter(wav, m_filterParams.m_baseFreq, m_filterParams.m_bIIR, m_filterParams.m_slope);
	else
		return Filter::filter(wav, symsMap);
}

bool CFilterDlg::FilterParams::operator==(const FilterParams & other) const
{
	if (m_bExpressionType != other.m_bExpressionType)
		return false;

	if (other.m_bExpressionType == WaveFilter::ParamType::FREQUENCY)
		return m_baseFreq == other.m_baseFreq && m_slope == other.m_slope && m_bIIR == other.m_bIIR;
	else
		return m_strExpression == other.m_strExpression;	
}


void CFilterDlg::OnEnChangeeditslope()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	if (m_spinSlope.m_hWnd)
	{
		BOOL bErr;
		int pos = m_spinSlope.GetPos32(&bErr);

		if (bErr)
			m_spinSlope.SetPos(pos);
	}
}
