#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include "WavTrack.h"
#include "libsrc\WavMeta.h"


class CMainDlg;
// RecDlg dialog

class RecDlg : public CDialog
{
	DECLARE_DYNAMIC(RecDlg)

public:
	RecDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~RecDlg();

// Dialog Data
	enum { IDD = IDD_RECORD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRec();
	afx_msg void OnBnClickedStop();
	afx_msg void OnBnClickedForm();
	afx_msg void OnBnClickedPlay();
	afx_msg void OnBnClickedWreck();
	afx_msg void OnBnClickedSave();
	afx_msg void OnEnUpdateBaseFreq();
//	afx_msg void OnNMCustomdrawWreck(NMHDR *pNMHDR, LRESULT *pResult);
//	afx_msg void OnBnSetfocusWreck();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedOk();


private:
	FORMATCHUNK m_formatChunk;
	HWAVEIN m_waveIn;
	HWAVEOUT m_waveOut;
	WAVEHDR m_waveHeader;
	WAVEFORMATEX m_wfx;
		
	BOOL m_bStereo;
	BOOL m_bFormat;
	
	CString m_strSampleRate;
	CString m_strBPS;
	CString m_strBaseFreq;

	int m_iVolume;

	std::vector<uint8_t> m_recordedSamples, m_wreckedSamples;
	std::vector<float> m_floatSamples;

	bool m_bHasTrack;
	bool m_bWrecked;

	double m_baseFreq;

	CComboBox m_cmbBPS;
	CEdit m_editBaseFreq;
	CMainDlg* m_dlgParent;
	HANDLE m_thrRecord;

	DecimalCorrector m_correctBaseFreq;

	static uint32_t Record(LPVOID pParam);
	void StopRecording();

	const std::vector<uint8_t>& getSamplesToPlay() const;
	CSliderCtrl m_sliderHarmonics;
};