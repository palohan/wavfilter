
// achjaj.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CachjajApp:
// See achjaj.cpp for the implementation of this class
//

class CachjajApp : public CWinApp
{
public:
	CachjajApp();

// Overrides
public:
	virtual BOOL InitInstance();
// Implementation

	//virtual int DoMessageBox(LPCTSTR lpszPrompt, UINT nType, UINT nIDPrompt) override;


	DECLARE_MESSAGE_MAP()
};

extern CachjajApp theApp;


#include <string>

std::string CStringToString(const CString& str);

bool validNumber(const CString& str, double limitDown = 0, double limitUp = 0, double* pValue = nullptr);

void initTextColor(CHARFORMAT2& format, COLORREF color);

void CorrectEditBox(CEdit& edit, CString& str);



class DecimalCorrector
{
	CEdit& m_control;
	CString& m_sText, m_sTip;

	double m_lowLimit = 0, m_highLimit = 0;
	double& m_resultVal;

public:
	DecimalCorrector(CEdit& control, CString& sText, double& resultVal, double lowLimit = 0, double highLimit = 0);
	void setLowLimit(double lowLimit);
	void setHighLimit(double highLimit);

	bool validate();
	bool checkBounds() const;
	void showTip(LPCWSTR balloonTitle, int icon = TTI_WARNING);
	void correct();

	double getValue() const;

	bool validateAndCorrect(UINT_PTR nIDEvent, UINT nElapse, LPCWSTR balloonTitle);

private:
	void createTip();
};
