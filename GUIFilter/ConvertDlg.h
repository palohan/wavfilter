#pragma once
#include "afxwin.h"

class CMainDlg;
// ConvertDlg dialog

class ConvertDlg : public CDialogEx
{
	DECLARE_DYNAMIC(ConvertDlg)

public:
	ConvertDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~ConvertDlg();

// Dialog Data
	enum { IDD = IDD_CONVERT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedConvert();
	afx_msg void OnCbnSelchangeCbps();
	afx_msg void OnBnClickedIsfloat();

private:
	CString m_strSavePath;
	CString m_strBPS;
	BOOL m_isFloat;
	CComboBox m_cmbBPS;
	CButton m_btIsFloat;
	CButton m_btConvert;
	CButton m_btLoadResult;
	CMainDlg* m_dlgParent;

	CString m_sResultFilePath;
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedLoadresult();
};
