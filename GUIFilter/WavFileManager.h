#pragma once

#include <vector>

class WavFileManager
{
public:
	WavFileManager() = default;

	typedef std::pair<CString, ULARGE_INTEGER> fileTimePair;
	
	bool isBad(const CString& filePath, fileTimePair* outPair = nullptr);

	void insert(const fileTimePair& item);
	void insert(const CString& path);

	void erase(const CString& path);

	auto findItem(const CString& filePath);

	fileTimePair getPair(const CString& filePath);

private:
	std::vector<fileTimePair> badWavs;
};

