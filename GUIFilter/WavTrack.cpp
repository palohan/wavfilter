#include "stdafx.h"

#include "WavTrack.h"
#include "libsrc/WavMeta.h"

using namespace std;

TrackControls::TrackControls(void) : m_bPaused(false), m_threadFunc(nullptr)
{
	m_mmTime.wType = TIME_BYTES;											//poloha prehravania v B
}

TrackControls::TrackControls(CWnd* btPlay, CWnd* btPause, CWnd* btStop, CString* strOutput, CWnd* pWnd) :
	m_btPlay(btPlay),
	m_btPause(btPause),
	m_btStop(btStop),
	m_strOutput(strOutput),
	m_pWindow(pWnd),
	m_bPaused(false),
	m_threadFunc(nullptr),
	m_buffer(nullptr),
	m_bufferLength(0),
	m_bPlayOrPause(true)
{
	assert(btPlay);
	assert(btPause);
	assert(btStop);
	assert(strOutput);
	assert(pWnd);


	m_mmTime.wType = TIME_BYTES;
}

TrackControls::~TrackControls(void)
{
}


void TrackControls::OnPlayButton()
{
	if (m_bPlayOrPause)
		Play();
	else
		Pause();
}


void TrackControls::InitWFX(WAVEFORMATEX & wfx, const FORMATCHUNK & fmtChunk, bool isFloat)
{
	ZeroMemory(&wfx, sizeof(wfx));					//only integral types -> ok

	wfx.nSamplesPerSec = fmtChunk.m_SampleRate;
	wfx.nChannels = fmtChunk.m_Channels;
	wfx.wBitsPerSample = fmtChunk.m_BitsPerSample;
	wfx.wFormatTag = fmtChunk.m_Format;

	if (isFloat)
	{
		wfx.wBitsPerSample = 32;
		wfx.wFormatTag = WavMeta::FLOAT;
	}

	wfx.nBlockAlign = (wfx.wBitsPerSample >> 3) * wfx.nChannels;
	wfx.nAvgBytesPerSec = wfx.nBlockAlign * wfx.nSamplesPerSec;
}

void TrackControls::InitWFX(const FORMATCHUNK& fmtChunk, bool isFloat)
{
	InitWFX(m_wfx, fmtChunk, isFloat);
}

void TrackControls::SetVolume(CSliderCtrl & slider)
{
	auto channelVol = slider.GetPos();
	SetVolume(MAKELONG(channelVol, channelVol));					//STRANGE - works for both tracks...
}

void TrackControls::SetVolume(DWORD volume)
{
	waveOutSetVolume(m_hWaveOut, volume);
}

void TrackControls::Play()
{
	if (!m_threadFunc || m_threadFunc->ResumeThread() == -1)
		m_threadFunc = AfxBeginThread(showTime, this, 0, 0, SYNCHRONIZE);
	
	m_btPlay->EnableWindow(FALSE);
	m_btPause->EnableWindow();
	//m_btPlay->SetWindowTextW(L"PAUSE");
	m_btStop->EnableWindow();
		
	if (!m_bPaused)													//ak neni pauznuta
		waveOutWrite(m_hWaveOut, &m_WaveHdr, sizeof(WAVEHDR));				//prehrava od zaciatku
	else														
	{
		waveOutRestart(m_hWaveOut);									//ak je pauznuta, restartne playback
		m_bPaused = false;
	}
}

void TrackControls::Pause()
{
	waveOutPause(m_hWaveOut);											//pauzne nove
	
	if (m_threadFunc)
		m_threadFunc->SuspendThread();
		
	//m_btPlay->SetWindowTextW(L"PLAY");
	m_btPlay->EnableWindow(TRUE);
	m_btPause->EnableWindow(FALSE);
	m_btStop->EnableWindow(TRUE);
	
	m_bPaused = true;													//aby sa resumla
}


void TrackControls::OnStop()
{
	waveOutReset(m_hWaveOut);											//stopne wav a da na zaciatok & resetne pocitadlo vzoriek	

	//m_btPlay->SetWindowTextW(L"PLAY");
	m_btPlay->EnableWindow(TRUE);
	m_btPause->EnableWindow(FALSE);
	m_btStop->EnableWindow(FALSE);
}


void TrackControls::Stop()
{
	OnStop();
	
	m_bPaused = false;												//ak bola pauznuta, uz neni
	DWORD dw = WaitForSingleObject(m_threadFunc, INFINITE);

	m_strOutput->Empty();											//vynulujeme pocitadlo priebehu

	m_pWindow->UpdateData(FALSE);								//zobrazime ho do staticu
}


UINT TrackControls::showTime(LPVOID _this)
{
	ASSERT(_this);
	TrackControls& This = *static_cast<TrackControls*>(_this);

	const double dwBufferLength = This.m_WaveHdr.dwBufferLength;
	/*const DWORD blockSize = 50000;
	static DWORD bytesLeft, offset;


	for (offset = 0; offset < This.m_bufferLength; offset += blockSize)
	{
		bytesLeft = This.m_bufferLength - offset;

		This.m_WaveHdr.lpData = This.m_buffer + offset;
		This.m_WaveHdr.dwBufferLength = bytesLeft < blockSize ? bytesLeft : blockSize;
		This.m_WaveHdr.dwFlags = 0;

		This.Prepare();
		waveOutWrite(This.m_hWaveOut, &This.m_WaveHdr, sizeof(WAVEHDR));*/
		do
		{
			waveOutGetPosition(This.m_hWaveOut, &This.m_mmTime, sizeof(MMTIME));
			This.m_strOutput->Format(L"%3.2f%%", 100.0*This.m_mmTime.u.cb / dwBufferLength);			//cb = byte count. progres prehravania: XXX.XX%

			This.m_pWindow->SendMessage(WM_APP_UPDATE);
		}
		while ((This.m_WaveHdr.dwFlags & WHDR_DONE) == 0);								//kym hra

		This.m_strOutput->Empty();									//progress can be stuck below 100%
		This.m_pWindow->SendMessage(WM_APP_UPDATE);

		/*waveOutUnprepareHeader(This.m_hWaveOut, &This.m_WaveHdr, sizeof(WAVEHDR));
	}
	offset = 0;*/
	This.OnStop();	
	return 0;
}


void TrackControls::setBuffer(LPSTR buffer, DWORD length)
{
	this->m_buffer = buffer;
	m_bufferLength = length;
}


UINT TrackControls::playTrack(LPVOID _this)
{
	ASSERT(_this);
	TrackControls& This = *static_cast<TrackControls*>(_this);

	const DWORD blockSize = 50000;
	static DWORD bytesLeft;

	for (static DWORD i = 0; i < This.m_bufferLength; i += blockSize)
	{
		bytesLeft = This.m_bufferLength - i;

		This.m_WaveHdr.lpData = This.m_buffer + blockSize;
		This.m_WaveHdr.dwBufferLength = bytesLeft < blockSize ? bytesLeft : blockSize;
		This.m_WaveHdr.dwFlags = 0;

		This.Prepare();
	}

	return 0;
}


MMRESULT TrackControls::Open()
{
	return waveOutOpen(&m_hWaveOut, WAVE_MAPPER, &m_wfx, 0, 0, CALLBACK_NULL);
}

MMRESULT TrackControls::Prepare(LPSTR lpData, DWORD dwBufferLength)
{
	ZeroMemory(&m_WaveHdr, sizeof(m_WaveHdr));

	//m_trackOriginal.SetVolume(m_sliderVolume);		//nastavi pociatocnu volume pre oba vystupy

	m_WaveHdr.lpData = lpData;
	m_WaveHdr.dwBufferLength = dwBufferLength;
	m_WaveHdr.dwFlags = 0;
	
	return Prepare();
}

MMRESULT TrackControls::Prepare()
{
	return waveOutPrepareHeader(m_hWaveOut, &m_WaveHdr, sizeof(WAVEHDR));
}

MMRESULT TrackControls::Close()
{
	waveOutReset(m_hWaveOut);				//Before calling waveOutClose, the application must wait for all buffers to finish playing or call the waveOutReset function to terminate playback.
	DWORD dw = WaitForSingleObject(m_threadFunc, INFINITE);
	return waveOutClose(m_hWaveOut);
}