// ConvertDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GUIFilter.h"
#include "ConvertDlg.h"
#include "afxdialogex.h"
#include "MainDlg.h"

#include "libsrc/Convert.h"


// ConvertDlg dialog

IMPLEMENT_DYNAMIC(ConvertDlg, CDialogEx)

ConvertDlg::ConvertDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(ConvertDlg::IDD, pParent)	
	, m_strBPS(_T(""))
	, m_isFloat(FALSE)
	, m_dlgParent((CMainDlg*) pParent)
{	
}


BOOL ConvertDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	//m_dlgParent = (CMainDlg*)GetParent();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}


ConvertDlg::~ConvertDlg()
{
}

void ConvertDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_CBString(pDX, IDC_CBPS, m_strBPS);
	DDX_Check(pDX, IDC_ISFLOAT, m_isFloat);
	DDX_Control(pDX, IDC_CBPS, m_cmbBPS);
	DDX_Control(pDX, IDC_CONVERT, m_btConvert);
	DDX_Control(pDX, IDC_LOADRESULT, m_btLoadResult);
}


BEGIN_MESSAGE_MAP(ConvertDlg, CDialogEx)
	ON_BN_CLICKED(IDC_CONVERT, &ConvertDlg::OnBnClickedConvert)
	ON_CBN_SELCHANGE(IDC_CBPS, &ConvertDlg::OnCbnSelchangeCbps)
	ON_BN_CLICKED(IDC_ISFLOAT, &ConvertDlg::OnBnClickedIsfloat)
	ON_BN_CLICKED(IDC_LOADRESULT, &ConvertDlg::OnBnClickedLoadresult)
END_MESSAGE_MAP()


// ConvertDlg message handlers


void ConvertDlg::OnBnClickedConvert()
{	
	UpdateData();
	
	CFileDialog dlgSave(FALSE, L".wav", NULL, FILE_DLG_ARGS);
	
	if (dlgSave.DoModal() == IDOK)
	{
		m_sResultFilePath = dlgSave.GetPathName();

		Convert::convertWav(m_dlgParent->m_wavMetaGood, CStringToString(m_sResultFilePath), StrToInt(m_strBPS), m_isFloat == 1 ? WavMeta::FLOAT : WavMeta::PCM);
		
		MessageBox(L"Done");
		m_btLoadResult.EnableWindow(TRUE);
	}
	else
		m_strSavePath.Empty();
}


void ConvertDlg::OnCbnSelchangeCbps()
{
	m_btConvert.EnableWindow(TRUE);
}


void ConvertDlg::OnBnClickedIsfloat()
{
	UpdateData();

	m_cmbBPS.EnableWindow(!m_isFloat);

	if (m_cmbBPS.GetCurSel() == -1)
		m_btConvert.EnableWindow(m_isFloat);
}


void ConvertDlg::OnBnClickedLoadresult()
{
	OnOK();
	m_dlgParent->LoadFile(m_sResultFilePath);
}
