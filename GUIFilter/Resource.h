//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GUIFilter.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAIN                        102
#define IDC_TOOLTIP_TIMER               105
#define IDR_MAINFRAME                   128
#define IDD_FILTER                      130
#define IDD_RECORD                      131
#define IDD_CONVERT                     133
#define IDC_OPEN                        1000
#define IDC_FILENAME                    1001
#define IDC_EditBaseFreq                1006
#define IDC_INFO                        1007
#define IDC_sliderVOLUME                1009
#define IDC_SAVE                        1011
#define IDC_FILTER                      1012
#define IDC_PLAYOLD                     1015
#define IDC_PAUSEOLD                    1016
#define IDC_STOPOLD                     1017
#define IDC_PLAYNEW                     1022
#define IDC_ST_PlayProgNew              1023
#define IDC_PAUSENEW                    1024
#define IDC_STOPNEW                     1025
#define IDC_editSlope                   1033
#define IDC_spinSlope                   1034
#define IDC_RECORD                      1044
#define IDC_BPS                         1045
#define IDC_FS                          1046
#define IDC_STEREO                      1047
#define IDC_STOP                        1049
#define IDC_WRECK                       1050
#define IDC_PLAY                        1052
#define IDC_BASE_FREQ                   1053
#define IDC_FORMAT                      1054
#define IDC_CONVERT                     1055
#define IDC_CBPS                        1056
#define IDC_ISFLOAT                     1057
#define IDC_HARMONIC_VOLUME             1058
#define IDC_RADIO_BASE_FREQ             1059
#define IDC_RADIO_FREQ_EXPR             1060
#define IDC_EDIT_CUST_EXPR              1062
#define IDC_CONV_DLG                    1066
#define IDC_LOADRESULT                  1067
#define IDC_checkIIR                    1069
#define IDC_ST_PlayProgOld              1070
#define IDC_EDIT_INVISIBLE              1071
#define IDC_RICHEDIT_EXPR_STATUS        1073
#define IDC_HARMONIC_COUNT              1074

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1075
#define _APS_NEXT_SYMED_VALUE           108
#endif
#endif
