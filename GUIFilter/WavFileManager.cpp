#include "stdafx.h"
#include "WavFileManager.h"

#include <algorithm>

using namespace std;


auto WavFileManager::findItem(const CString& filePath)
{
	return std::find_if(badWavs.begin(), badWavs.end(), [&filePath](const fileTimePair& arg)
	{
		return filePath == arg.first;
	});
}


ULARGE_INTEGER GetLastWriteTime(const WIN32_FIND_DATA& FindFileData)
{
	ULARGE_INTEGER time;
	
	time.LowPart = FindFileData.ftLastWriteTime.dwLowDateTime;
	time.HighPart = FindFileData.ftLastWriteTime.dwHighDateTime;
	
	return time;
}


bool WavFileManager::isBad(const CString& filePath, fileTimePair* outPair)
{
	fileTimePair newItem = getPair(filePath);

	if (outPair)
		*outPair = newItem;

	if (newItem.second.QuadPart == -1)
    {
		insert(newItem);
        return true;
    }  

	auto iter = findItem(filePath);

	if (iter != badWavs.end())										//ak ju naslo
		if (newItem.second.QuadPart == iter->second.QuadPart)		//ak je rovnaka, je zla
			return true;
		else
			badWavs.erase(iter);

	return false;													//ak ju nenaslo, neni zla
}


WavFileManager::fileTimePair WavFileManager::getPair(const CString& filePath)
{	    
	WIN32_FIND_DATA FindFileData;
    HANDLE hFile = FindFirstFile(filePath, &FindFileData);

	ULARGE_INTEGER time;

    if (hFile == INVALID_HANDLE_VALUE)
		time.QuadPart = -1;
	else
		time = GetLastWriteTime(FindFileData);
	
	FindClose(hFile);
		
	return make_pair(filePath, time);
}


void WavFileManager::insert(const CString& path)
{
	insert(getPair(path));
}

void WavFileManager::insert(const fileTimePair& newItem)
{
	auto iter = findItem(newItem.first);
	
	if (iter == badWavs.end())
		badWavs.push_back(newItem);								//insert if not exists
	else
		if (newItem.second.QuadPart != iter->second.QuadPart)
			iter->second.QuadPart = newItem.second.QuadPart;	//update if exists
}


void WavFileManager::erase(const CString& path)
{
	auto iter = findItem(path);

	if (iter != badWavs.end())
		badWavs.erase(iter);
}