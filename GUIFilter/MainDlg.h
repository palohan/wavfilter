
// achjajDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "filterdlg.h"
#include "recdlg.h"
#include "ConvertDlg.h"
#include "WavFileManager.h"



// CMainDlg dialog
class CMainDlg : public CDialogEx
{
// Construction
public:
	CMainDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MAIN };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions

public:
	virtual BOOL OnInitDialog();
protected:
	afx_msg void OnSysCommand(uint32_t nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBrowse();
	afx_msg void OnBnClickedInfo();
	afx_msg void OnBnClickedSave();
	afx_msg void OnBnClickedFilter();
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedPLAYOLD();
	afx_msg void OnBnClickedPAUSEOLD();
	afx_msg void OnBnClickedSTOPOLD();
	afx_msg void OnBnClickedPlaynew();
	afx_msg void OnBnClickedPausenew();
	afx_msg void OnBnClickedStopnew();
	afx_msg void OnBnClickedRecord();
	afx_msg void OnBnClickedConvDlg();
	afx_msg void OnHScroll(uint32_t nSBCode, uint32_t nPos, CScrollBar* pScrollBar);
	

	WavMeta m_wavMetaGood;

	void LoadFile(const CString& filePath);
	void LoadWav(WavMeta& wavMeta);

private:
	WavFileManager wavFileManager;
	bool m_bPlaybacksReady, m_bHadGoodPlaybacks, saved;
	
	RecDlg m_dlgRecord;
	ConvertDlg m_dlgConvert;
	CFilterDlg m_dlgFilter;

	CButton m_btFilter;

	TrackControls m_trackOriginal, m_trackFiltered;

	vector<float> m_vecFiltered;	

	//Playable m_oldTrack, m_newTrack;
	
	CSliderCtrl m_sliderVolume;

	CString m_strOrigProgress, m_strFilteredProgress, m_lastGoodPath, m_strDisplayFileName;
	
	LRESULT UpdateData_Thread(WPARAM wpD, LPARAM lpD);	
	void updateControlsState(BOOL UP);
	bool PrepareWav(WavMeta& wavMeta);
};
