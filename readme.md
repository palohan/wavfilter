# Introduction #

This repo contains a GUI and CLI of an impulse response digital filter for WAV files. Specify the input WAV, filter out the defective signal and save the output track.

# Background

I took a DSP & coding class back in summer semester of 2012. To complete an assignment in both, I took up a task of creating a comb fitler in C++. This project helped me overcome my big initial fear of coding (which I think puts off a lot of people) - back then I knew only a little bit of C. I was really green back then - didn't know about dynamic memory, const refs and even debugging existed! I couldn't count the number of hours I spent on it - every little thing I learned I had to try here. Every line has been visited & revisited many times over the years - **the project reflects my coding skill (as of May 2017) pretty well.** 

To conclude: Had I given up on this, I would not be coding today.

As for the technologies, I LOVE C++, especially the new hot flavors of it. I didn't enjoy MFC that much, especially after I learned winforms, WPF & web existed.

# Terminology
- FF - fundamental frequency
- FS - sampling frequency
- BPS - bits per sample
- Fmax - maximum possible frequency under current FS = FS/2
- Comb filter - filters out all the harmonics of a periodic signal with given FF
- FIR - finite impulse response - each output sample is a product of filter's coefficients and input samples preceding it
- IIR - infinite impulse response - each output sample is a product of filter's coefficients and input AND output samples preceding it
- WAV formats:
	- PCM - samples stored in integers of a particular size (BPS):
		- 8b unsigned
		- 16b, 24b, 32b signed
	- FLOAT - 4B float number

# Building

This solution uses MFC, so please use Visual Studio 2017, which is free in Community version.

# Repo & Solution layout

This repo contains a subtree called libfilter, which can be built via CMake into a self contained CLI filter. For more information, see my libfilter repo. GUI and CLI consume this libfilter as a dll. 

The solution is organized into these projects:

- libfilterProj - dll for filtering, converting and WAV IO; consumed as dll used in CLI & GUI. Written in starnard C++17 (uses std::experimental::filesystem).
- GUIFilter - written in MFC; supports playing, filtering, converting, recording & saving WAVs. usage is described below
- CLIFilter - console program; usage is described below

## GUIFilter description

Here follows the description of the GUI elements:

- WAV Filter - the main dialog, navigates to other dialogs:
	- OPEN - loads the selected WAV file as the original track. You can find sample WAVs in ./samples/ folder.
	- RECORD - opens up the RECORD dialog. Continuous track saving is NOT yet supported - might run out of memory on long recordings! Dialog has 2 sections: 
		- TOP: records a track
			- set BPS; float checkbox presets FLOAT32 format
			- set sample rate; optionally set stereo
			- RECORD - starts recording
			- PLAY - plays the track
			- STOP - stops recording / playback
		- BOTTOM: add a sine wave to the track
			- set base frequency in range <100, FS/2>
			- volume in range <0,100>
			- number of harmonics - min is 1, max is all up to FS/2
			- ADD IT - adds wave to recorded track
			- EXPORT - loads the track as the original track of the main dialog
			- SAVE - saves the track
			
	- CONVERT - opens a CONVERT dialog, where you can convert the original track to a different BPS or format
		- select BPS, or check FLOAT		
		- CONVERT - pick a save path
		- EXPORT - export the converted file to main dlg
		
	- FILTER - opens the FILTER dialog for specifying the filtering parameters. Produces a filtered track if the filtration succeeds. The parameters are specified in 2 ways:
		- individual parameters: they determine the fitlering coefficients, which will be stored in a text file under ./logs/ folder.
			- set Fundamental Frequency decimal in range of <100, sample rate / 2>
			- IIR - if checked, employs IIR; additionally, set Pole Height in permille <1-999>
		- Filter Coefficients: a polynomial in form of "1x^18 + ... + 3x^0 - 5y^18 - ... -2y^0"
			- either type them, or drag a text file containing them
		- OK - first validates the parameters; if good, filtering begins, which may take a few seconds. If successful, filtered track is accessible from the main dialog. 
		
	- PLAY OLD, PAUSE OLD, STOP OLD - playback for the original track
	- PLAY NEW, PAUSE NEW, STOP NEW - playback for filtered track
	- SAVE - saves the filtered track; you have a choice between original tracks's WAVE format and FLOAT format
	- INFO - properties of the original track
	- Volume slider - sets the audio volume
		
		
## CLIFilter description
```
#!

usage:
clifilter -i <input path> (-f <frequency> [-I <slope>] | --c <coeffs> | -C <coeffs path>) [-n] -o <output path>
run tests:
clifilter -t

Options:
        -i, --input             path to input WAV
        -f, --frequency         fundamental frequency of the unwanted signal <100, FS/2>
        -I, --IIR               sets IIR mode and its slope (0, 1). Otherwise, the mode is FIR 
        -c, --coeffs            quoted string of filtering coefficients - a polynomial in form of "1x^18 + ... + 3x^0 - 5y^18 - ... -2y^0"
        -C, --CoeffsPath        path to the file containing filtering coefficients
        -n, --nativeFormat      saves the output WAV in input's format; otherwise in FLOAT32
        -o, --output            path to output WAV 
		-t, --test				runs predefined tests: input WAVs are loaded from ./samples/, logs and results are written to ./logs/ and ./results/ respectively
```

### License ###

You are free to use this in whatever non-money-making way. I hold NO responsibility for any damages.

### The Future ###

MFC is clunky - I don't really want to extend this app anymore. I tried my best, but if you find some bugs, please report them.

### Contact ###

I am open to any suggestions. I can't promise speedy replies though.

# Have fun! #