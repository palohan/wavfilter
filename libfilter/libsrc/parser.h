#pragma once


#include <string>
#include <map>
#include "sym.h"


class ParserException : public std::exception
{
	std::string m_what;

	void createMsg();

public:
	CLASS_API ParserException(int badChar, std::streampos position, const char* reason);
	CLASS_API ParserException(std::istream& stream, const char* reason);

	char m_badChar;
	const char * const m_reason;
	std::streampos m_position;

	CLASS_API const char* what() const noexcept override;
};


class CLASS_API Parser
{
public:
	typedef std::map<char, vector<sym<double>>> vectorMap;

	Parser::vectorMap getVectorsMap(std::istream& m_stream);

	void showVectorsMap(const vectorMap& vecMap) const;

private:
	vectorMap buildMap(vector<sym<double>>& syms);
};

