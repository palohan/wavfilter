#pragma once

#include <map>
#include <vector>

#include <experimental/filesystem>
using namespace std::experimental::filesystem;

enum class WavStatus
{
	GENERIC_ERROR = -1,
	WAV_FILE_OK = 0,
	BAD_CHANNEL,
	BAD_FORMAT,
	BAD_BIT_RATE,
	BAD_HEADER,
	FILE_NOT_OPEN,
	FILE_EOF
};


struct ChunkMeta
{
	char m_ID[4] = { 0 };			//"fmt " / "data"
	uint32_t m_size = 0;				//byte size of remaining data
};

struct CLASS_API FORMATCHUNK
{
	enum Channel
	{
		MONO = 1,
		STEREO = 2,
		LAST = STEREO
	};

	uint16_t		m_Format = 0;				// Audio format 1=PCM, 6=mulaw, 7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM
	uint16_t		m_Channels = 0;				// Number of channels 1=Mono 2=Stereo
	uint32_t		m_SampleRate = 0;			// Sampling Frequency [Hz]
	uint32_t		m_ByteRate = 0;				// sample size * channels * samples per sec [B]
	uint16_t		m_BlockAlign = 0;			// sample size * channels [B]
	uint16_t		m_BitsPerSample = 0;		// bits per sample

	FORMATCHUNK() = default;
	FORMATCHUNK(uint16_t bps, uint16_t format, uint32_t fs, uint16_t channels);
		
	std::string print() const;

	void reCalculate(bool toFloat = false);

	bool isFloat() const;	

	void validate() const;

	std::string getFormatInfo() const;
};

class Playable
{
	FORMATCHUNK* m_formatChunk;

	std::vector<float>* m_floatVec;
	std::vector<uint8_t>* m_byteVec;

	Playable(FORMATCHUNK& formatChunk, std::vector<float>& floatVec, std::vector<uint8_t>& byteVec);

};

struct CLASS_API RIFFCHUNK
{
	char					WAVE[4] = { 0 };        // WAVE Header
};

struct CLASS_API WavHeader
{
	ChunkMeta				m_riffMeta = {};		//8B FMT header, RIFF Header
	RIFFCHUNK				m_riffChunk = {};	//12B
	ChunkMeta				m_fmtMeta = {};		//8B FMT header, FORMATCHUNK Size[B]
	FORMATCHUNK				m_fmtChunk = {};		//16B
	ChunkMeta				m_dataMeta = {};		//8B "data"  string, Sampled data length [B]	

	static const size_t RiffSizeWithoutData;

	void preFill();
	bool isRiffValid() const;
};

class WavException;
class WavMeta
{
	size_t
		m_totalSamples = 0,
		m_totalSamplesPerChannel = 0,
		m_trackTime = 0;					//[ms]

	uintmax_t m_fileSize = 0;

	std::vector<float> m_floatVec;
	std::vector<uint8_t> m_byteVec;
	
	WavStatus m_wavStatus = {};

public:
	WavHeader m_wavHead = {};

	CLASS_API WavMeta() = default;
	CLASS_API WavMeta(const path& filePath);
	CLASS_API WavMeta(const FORMATCHUNK& formatChunk, const std::vector<uint8_t>& vecSamples);
	
	//void printInfo();

	CLASS_API static const char* getVawFormat(uint16_t format);

	CLASS_API void loadFile(const path& filePath);
	

	CLASS_API size_t getTotalSamples() const;
	CLASS_API size_t getTotalSamplesInChannel() const;
	CLASS_API size_t getTrackTime() const;
	CLASS_API uintmax_t getFileSize() const;

	CLASS_API const std::vector<float>& getFloatVec() const;
	CLASS_API const std::vector<uint8_t>& getByteVec() const;
	

	enum WavFormat
	{
		UNKNOWN = 0,
		PCM = 1,
		FLOAT = 3,
		mulaw = 6,
		alaw = 7,
	};

	//const WavHeader& m_wavHead const;

private:
	void exitOnError(WavStatus code, const std::string& msg = "");
	void calculateTotals();

	static const std::map<uint16_t, const char*> vawFormats;
};



class WavException : public std::logic_error
{
	WavStatus m_status;
	
	static const std::map<WavStatus, const char*> statusMsgMap;
	static const char* getStatusInfo(WavStatus status);

public:
	CLASS_API WavException(WavStatus status, const std::string& msg = "");

	CLASS_API WavStatus getStatus() const;
};
