
#include "parser.h"

#include <iostream>
#include <algorithm>

using namespace std;


Parser::vectorMap Parser::getVectorsMap(std::istream& stream)
{
	stream >> skipws >> ws;	
	
	vector<sym<double>> syms;

	while (stream.good() && !stream.eof())
	{
		sym<double> s;

		double fVal = 0;
		char cVar = 0, cExpSign = 0;
		int iExp = 1;
		

		if (!(stream >> fVal))				//read coef [float]
			throw ParserException(stream, "Failed to read scalar [double]!");				//bad / missing double


		if (stream >> cVar)					//read variable [letter]
		{
			if (!isalpha(cVar))
				throw ParserException(cVar, stream.tellg().operator-(1), "Variable must be a letter!");
		}
		else
			throw ParserException(stream, "Variable [alphabetic char] is missing!");
		
	
		if (stream >> cExpSign)				//try reading ^
		{
			if (cExpSign == '^')
			{
				if (!(stream >> iExp))
					throw ParserException(stream, "Failed to read exponent [int]!");
			}
			else
				throw ParserException(cExpSign, stream.tellg().operator-(1), "Variable must be followed with '^' sign!");		//bad ^
		}
		else
			throw ParserException(stream, "Variable must be followed with '^' sign!");		//missing ^

		
		ws(stream);

		syms.push_back(sym<double>(fVal, cVar, iExp));
	}

	return buildMap(syms);
}


Parser::vectorMap Parser::buildMap(vector<sym<double>>& syms)
{
	vectorMap symsMap;

	for (auto& sym : syms)
		symsMap[sym.m_var].push_back(sym);
	
	return symsMap;
}


void Parser::showVectorsMap(const vectorMap & vecMap) const
{
	for (auto& symsMapVal : vecMap)
	{
		printf("var: %c\n", symsMapVal.first);		
		vectorMap::mapped_type::value_type::showVector(symsMapVal.second);
	}
}


ParserException::ParserException(int badChar, std::streampos position, const char * reason) :
	m_badChar(badChar), 
	m_reason(reason),
	m_position(position)
{
	createMsg();
}

ParserException::ParserException(std::istream& stream, const char * reason) :
	m_reason(reason)
{
	auto state = stream.rdstate();	
	stream.clear();

	m_badChar = ws(stream).peek();
	m_position = stream.tellg();

	stream.setstate(state);				//restore stream state

	createMsg();
}

void ParserException::createMsg()
{
	std::stringstream ss;
	ss << "ParserException: " << "Invalid character: '" << m_badChar << "' at position: " << m_position << endl << "Reason: " << m_reason << endl;
	m_what = ss.str();
}

const char * ParserException::what() const noexcept
{
	return m_what.data();
}